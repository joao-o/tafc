#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* holds psi,pi and phi */
typedef struct w3{
	double ps,pi,ph;
} w3;


void rhsper(w3 *f,w3 *y,int N,double dx) /*periodic boundary codintions*/
/* compute rhs for the 3N equations*/
{
	int i;
	/* left boundary*/
	f[0].ps=y[0].pi;
	f[0].pi=0.5*(y[N-1].ph-y[1].ph)/dx;
	f[0].ph=0.5*(y[N-1].pi-y[1].pi)/dx;

	/*middle*/
	for (i=1;i<N-1;i++){
		f[i].ps=y[i].pi;
		f[i].pi=0.5*(y[i-1].ph-y[i+1].ph)/dx;
		f[i].ph=0.5*(y[i-1].pi-y[i+1].pi)/dx;
	}

	/* right boundary*/
	f[N-1].ps=y[N-1].pi;
	f[N-1].pi=0.5*(y[N-2].ph-y[0].ph)/dx;
	f[N-1].ph=0.5*(y[N-2].pi-y[0].pi)/dx;
}


void rhsclosed(w3 *f,w3 *y,int N,double dx)  /*open boundary conditions*/
/* compute rhs for the 3N equations*/
{
	int i;
	/* left boundary*/
	f[0].ps=y[0].pi;
	f[0].pi=0;
	f[0].ph=-(1.5*y[0].pi+2*y[1].pi+0.5*y[2].pi)/dx;

	for (i=1;i<N-1;i++){
		f[i].ps=y[i].pi;
		f[i].pi=0.5*(y[i-1].ph-y[i+1].ph)/dx;
		f[i].ph=0.5*(y[i-1].pi-y[i+1].pi)/dx;
	}

	/* right boundary*/
	f[N-1].ps=y[N-1].pi;
	f[N-1].pi=0;
	f[N-1].ph=-(1.5*y[N-1].pi-2*y[N-2].pi+0.5*y[N-3].pi)/dx;
}

void rhsopen(w3 *f,w3 *y,int N,double dx)  /*open boundary conditions*/
/* compute rhs for the 3N equations*/
{
	int i;
	/* left boundary*/
	f[0].ps=y[0].pi;
	f[0].pi=(1.5*y[0].ph-2*y[1].ph+0.5*y[2].ph)/dx;
	f[0].ph=0;

	for (i=1;i<N-1;i++){
		f[i].ps=y[i].pi;
		f[i].pi=0.5*(y[i-1].ph-y[i+1].ph)/dx;
		f[i].ph=0.5*(y[i-1].pi-y[i+1].pi)/dx;
	}

	/* right boundary*/
	f[N-1].ps=y[N-1].pi;
	f[N-1].pi=-(1.5*y[N-1].ph-2*y[N-2].ph+0.5*y[N-3].ph)/dx;
	f[N-1].ph=0;
}

void rhsout(w3 *f,w3 *y,int N,double dx)  /*outgoing boundary conditions*/
/* compute rhs for the 3N equations*/
{

	int i;
	/* left boundary*/
	f[0].ps=y[0].pi;
	f[0].pi=1.0*(+(1.5*y[0].ph-2*y[1].ph+0.5*y[2].ph)/dx 
	             -(1.5*y[0].pi-2*y[1].pi+0.5*y[2].pi)/dx);
	f[0].ph=1.0*(-(1.5*y[0].ph-2*y[1].ph+0.5*y[2].ph)/dx
	             +(1.5*y[0].pi-2*y[1].pi+0.5*y[2].pi)/dx);

	for (i=1;i<N-1;i++){
		f[i].ps=y[i].pi;
		f[i].pi=0.5*(y[i-1].ph-y[i+1].ph)/dx;
		f[i].ph=0.5*(y[i-1].pi-y[i+1].pi)/dx;
	}

	/* right boundary*/

	f[N-1].ps=y[N-1].pi;
	f[N-1].pi=1.0*(-(1.5*y[N-1].ph-2*y[N-2].ph+0.5*y[N-3].ph)/dx 
	               -(1.5*y[N-1].pi-2*y[N-2].pi+0.5*y[N-3].pi)/dx);
	f[N-1].ph=1.0*(-(1.5*y[N-1].ph-2*y[N-2].ph+0.5*y[N-3].ph)/dx 
			         -(1.5*y[N-1].pi-2*y[N-2].pi+0.5*y[N-3].pi)/dx);
}

void rk4(w3 *y, void (*f)(w3 *, w3 *, int, double),int N,double h,double dx)
/* do runge kutta fourth order*/
{
	int i;
	w3 *k1,*k2,*k3,*k4;
	w3 *yt;

	k1=malloc(N*sizeof(w3));
	k2=malloc(N*sizeof(w3));
	k3=malloc(N*sizeof(w3));
	k4=malloc(N*sizeof(w3));
	yt=malloc(N*sizeof(w3));

	f(k1,y,N,dx); /* compute k1*/

	for(i=0;i<N;i++){ 
		/* compute y for computation of k2 */
		/* t can be ignored as the equations do not depend upon it*/
		yt[i].ps=y[i].ps+k1[i].ps*h*0.5;
		yt[i].pi=y[i].pi+k1[i].pi*h*0.5;
		yt[i].ph=y[i].ph+k1[i].ph*h*0.5;
	}
	f(k2,yt,N,dx);

	for(i=0;i<N;i++){
		yt[i].ps=y[i].ps+k2[i].ps*h*0.5;
		yt[i].pi=y[i].pi+k2[i].pi*h*0.5;
		yt[i].ph=y[i].ph+k2[i].ph*h*0.5;
	}
	f(k3,yt,N,dx);

	for(i=0;i<N;i++){
		yt[i].ps=y[i].ps+k3[i].ps*h;
		yt[i].pi=y[i].pi+k3[i].pi*h;
		yt[i].ph=y[i].ph+k3[i].ph*h;
	}
	f(k4,yt,N,dx);

	/*finally update y*/
	for(i=0;i<N;i++){
		y[i].ps+=(k1[i].ps + 2.0*k2[i].ps + 2.0*k3[i].ps + k4[i].ps)*h/6.0;
		y[i].pi+=(k1[i].pi + 2.0*k2[i].pi + 2.0*k3[i].pi + k4[i].pi)*h/6.0;
		y[i].ph+=(k1[i].ph + 2.0*k2[i].ph + 2.0*k3[i].ph + k4[i].ph)*h/6.0;
	} 
	free(k1);
	free(k2);
	free(k3);
	free(k4);
	free(yt);
}


int main(int argc, char **argv)
{
	int i;
	w3 *y;           
	int N=1000;     /* number of lines */
	double dx=0.01; /* distance between lines */
	double h=0.01;  /* timestep */
	double s=0.5;   /* sigma for initial gaussian distribuition*/
	double x0=5;    /* average for ""        ""             "" */
	double tf=10;   /* simulation stop time */
	double k=1.0;
	void (*f)(w3 *,w3 *, int, double);
	double t;


	/*Error message*/
	if (argc < 9){
		fprintf(stderr,"./wave <number of lines> <dx> <timestep>"
		        "<sigma> <x0> <tf> <k> <bc>\n"
		        "periodic bc =  p \n"
				  "open     bc =  o \n"
				  "closed   bc =  c \n"
				  "outgoing bc =  u \n");
		exit(EXIT_FAILURE);
	
	}

	/*Takes user inputed variables*/
	N = (int)strtol(argv[1], NULL, 10);
	dx = strtod(argv[2], NULL);
	h = strtod(argv[3], NULL);
	s = strtod(argv[4], NULL);
	x0 = strtod(argv[5], NULL);
	tf = strtod(argv[6], NULL);
	k = strtod(argv[7], NULL);


	/*Selection of boundary conditions*/
	switch(argv[8][0]){   
		case 'c':
			f = &rhsclosed;
			break;
		case 'o':
			f = &rhsopen;
			break;
		case 'p': 
			f = &rhsper;
			break;
		case 'u': 
			f = &rhsout;
			break;
		default:
			f=&rhsper;
	}
	
	y=malloc(N*sizeof(w3));
	
	/* initial conditions */
	for(i=0;i<N;i++){
		y[i].ps=exp(-0.5*(i*dx-x0)*(i*dx-x0)/s/s);
		y[i].ph=y[i].ps*(i*dx-x0)/s/s;
		y[i].pi=y[i].ph*k;
	}
	/*Prints the initial conditions*/
	for (i=0;i<N;i++) 
			printf("%e %e\n",dx*i,y[i].ps);
		printf("\n\n");


	/*Main loop*/
	for(t=0.0;t<tf;t+=h){
		/*Advances the system*/
		rk4(y,f,N,h,dx);
		for (i=0;i<N;i++)
			/*Saves the result*/
			printf("%e %e\n",dx*i,y[i].ps);
		printf("\n\n");
	}
	return 0;
}

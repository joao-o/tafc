#!/bin/sh

make
./wave 1000 0.01 0.005 0.5 5 20  0 p > data
gnuplot plot.gp
mv out.gif p-k0.gif
echo 1
./wave 1000 0.01 0.005 0.5 5 20  1 p > data
gnuplot plot.gp
mv out.gif p-k1.gif
echo 2
./wave 1000 0.01 0.005 0.5 5 20 -1 p > data
gnuplot plot.gp
mv out.gif p-k-1.gif
echo 3

./wave 1000 0.01 0.005 0.5 5 20  0 o > data
gnuplot plot.gp
mv out.gif o-k0.gif
echo 4
./wave 1000 0.01 0.005 0.5 5 20  1 o > data
gnuplot plot.gp
mv out.gif o-k1.gif
echo 5
./wave 1000 0.01 0.005 0.5 5 20 -1 o > data
gnuplot plot.gp
mv out.gif o-k-1.gif
echo 6

./wave 1000 0.01 0.005 0.5 5 20  0 c > data
gnuplot plot.gp
mv out.gif c-k0.gif
echo 7
./wave 1000 0.01 0.005 0.5 5 20 -1 c > data
gnuplot plot.gp
mv out.gif c-k-1.gif
echo 8
./wave 1000 0.01 0.005 0.5 5 20  1 c > data
gnuplot plot.gp
mv out.gif c-k1.gif
echo 9

./wave 1000 0.01 0.005 0.5 5 20  0 u > data
gnuplot plot.gp
mv out.gif u-k0.gif
echo 10
./wave 1000 0.01 0.005 0.5 5 20 -1 u > data
gnuplot plot.gp
mv out.gif u-k-1.gif
echo 11
./wave 1000 0.01 0.005 0.5 5 20  1 u > data
gnuplot plot.gp
mv out.gif u-k1.gif
echo 12

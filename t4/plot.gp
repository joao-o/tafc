set terminal gif animate delay 4
set output "out.gif"

set xrange [0:10]
set yrange [-1.1:1.1]

do for [i=1:200] {
	plot "data" index 20*i u 1:2 w lines notitle
}

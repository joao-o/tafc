#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(){
	int N= 500;
	double tmax = 1;
	double emin = 5e-5;
	double e;
	struct timespec cnow, cthen,rnow,rthen;
	double wall,cpu;

	double* matc = calloc(N*N,sizeof(double));
	double* matp = calloc(N*N,sizeof(double));
	double* evec = calloc(N*N,sizeof(double));

	for(int i=0;i<N;i++){
		matp[i+(N*(N-1))] = (i*tmax)/((double)N-1); 
		matc[i+(N*(N-1))] = (i*tmax)/((double)N-1); 
	}
	
	for(int i=0;i<N;i++){
		matp[i*N + (N-1)] = (i*tmax)/((double)N-1);
		matc[i*N + (N-1)] = (i*tmax)/((double)N-1);
	}

   clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cthen);
  	clock_gettime(CLOCK_REALTIME, &rthen);
	int it=0;
	do {
			#pragma omp parallel for 
			for(int j=1;j<N-1;j++){
				for (int i=1;i<N-1;i++){
					matc[i+N*j] = (matp[i+1+N*j]   + matp[i-1+N*j] +
						        matp[i + N+N*j] + matp[i - N+N*j])/4;
					evec[i+N*j]=fabs(matc[i+N*j]-matp[i+N*j]);
				}
			}

		e=evec[0];
		for(int i=1;i<N*N;i++)
			if(evec[i]>e)
				e=evec[i];

		double *temp=matc;
		matc=matp;
		matp=temp;
		it+=1;
		//fprintf(stderr,"%e\n",e);
	} while (e > emin);

  	clock_gettime(CLOCK_REALTIME, &rnow);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cnow);

	cpu =(cnow.tv_nsec-cthen.tv_nsec)*1e-6+(cnow.tv_sec-cthen.tv_sec)*1e3;
	wall=(rnow.tv_nsec-rthen.tv_nsec)*1e-6+(rnow.tv_sec-rthen.tv_sec)*1e3;

	fprintf(stderr,"%.10e %.10e %d\n",cpu,wall,it);

	for(int j=0;j<N;j++){
		for (int i=0;i<N;i++){
			printf("%e ", matp[i+N*j] );		
		}
		printf("\n");
	}

}

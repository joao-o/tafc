#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(){
	int N= 500;
	double tmax = 1;
	int i = 0; 
	int j = 0;
	int it = 0;
	double emin = 5e-5;
	double e;
	struct timespec cnow, cthen,rnow,rthen;
	double wall,cpu;

	double* matc = calloc(N*N,sizeof(double));
	double* matp = calloc(N*N,sizeof(double));
	double* evec = calloc(N*N,sizeof(double));

	/*For the bottom row*/

	for(i=0;i<N;i++){
		matp[i+(N*(N-1))] = (i*tmax)/((double)N-1); 
		matc[i+(N*(N-1))] = (i*tmax)/((double)N-1); 
	}

	/*For the rightmost column*/
	
	for(i=0;i<N;i++){
		matp[i*N + (N-1)] = (i*tmax)/((double)N-1);
		matc[i*N + (N-1)] = (i*tmax)/((double)N-1);
	}



	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cthen);
	clock_gettime(CLOCK_REALTIME, &rthen);

	do {


		double *auxc=matc;
		double *auxp=matp;

		for(j=1;j<N-1;j++){
			auxc+=N;
			auxp+=N;
			for (i=1;i<N-1;i++){
				auxc[i] = (auxp[i+1]   + auxp[i-1] +
				           auxp[i + N] + auxp[i - N])/4;
				evec[i+N*j]=fabs(auxc[i]-auxp[i]);
			}
		}

		e=evec[0];
		for(i=1;i<N*N;i++)
			if(evec[i]>e)
				e=evec[i];

		double *temp=matc;
		matc=matp;
		matp=temp;
	it +=1;

		//fprintf(stderr,"%e\n",e);
	} while (e > emin);



  	clock_gettime(CLOCK_REALTIME, &rnow);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cnow);

	cpu =(cnow.tv_nsec-cthen.tv_nsec)*1e-6+(cnow.tv_sec-cthen.tv_sec)*1e3;
	wall=(rnow.tv_nsec-rthen.tv_nsec)*1e-6+(rnow.tv_sec-rthen.tv_sec)*1e3;

	fprintf(stderr,"%.10e %.10e\n",cpu,wall);
	fprintf(stderr,"%d %.10e\n",it,e);

	for(j=0;j<N;j++){
		for (i=0;i<N;i++){
			printf("%d %d %e \n", i, j, matp[i+N*j] );		
		}
	}

}

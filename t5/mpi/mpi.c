#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <mpi.h>


int main(int argc, char* argv[]){
	int N= 500;
	double tmax = 1;
	int i = 0; 
	int j = 0;
	int t_height;
	double emin = tmax * 5e-5;
	double e; //e_all is the global error for the process
	struct timespec cnow, cthen,rnow,rthen;
	double wall,cpu;
	int rank, size;
	int it = 0;
	double e_all = tmax;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	//correction done to keep the size of the working space constant in each thread

	if(((N-2)%size)!= 0){

		N = N + ((N-2)%size);
		fprintf(stderr, "%d\n", N);

	}


	t_height = (N-2)/size; // height of the working in each thread

	//fprintf(stderr,"I'm process %d of %d \n", rank, size);


	/*N*(N) total matrix */
	/*i+1 goes to the right, i+N goes down */
	/*2*N is added  for the ghost cells*/

	double* matc = calloc(N*(t_height) + 2*N,sizeof(double)); //arrays where the system is kept
	double* matp = calloc(N*(t_height) + 2*N,sizeof(double));

	double* evec = calloc(N*(t_height) + 2*N,sizeof(double)); //array for the errors

	double* border_d = calloc(N,sizeof(double)); //up and down borders
	double* border_u = calloc(N,sizeof(double));
	double* ghost_d = calloc(N,sizeof(double)); //up and down ghost cells
	double* ghost_u = calloc(N,sizeof(double));


	/*initial conditions */

	/*For the rightmost row*/

	for(i=1;i<t_height +1;i++){
		matp[i*N + (N-1)] = ((i-1 + t_height*rank)*tmax)/((double)N-1); 
		matc[i*N +(N-1)] = ((i-1 + t_height*rank)*tmax)/((double)N-1); 
	}



	/*For the bottom row*/
	
	if(rank == size - 1){
		for(i=0;i<N;i++){
			matp[i+N*(t_height+1)] = (i*tmax)/((double)N-1); 
			matc[i+N*(t_height+1)] = (i*tmax)/((double)N-1); 
		}
	}	
	

	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cthen);
	clock_gettime(CLOCK_REALTIME, &rthen);

	//main loop

	do{
	    MPI_Request d_rec, u_rec, d_send, u_send; //setting the variables for the 
	    MPI_Status status;										  //ghost cell updating

		double *auxc=matc;
		double *auxp=matp;
		for(j=1;j<t_height+1;j++){ //skipping the borders  
			auxc+=N;  
			auxp+=N;
			for (i=1;i<N-1;i++){	//jacobi integration
				auxc[i] = (auxp[i+1]   + auxp[i-1] +
				           auxp[i + N] + auxp[i -N])/4;
				evec[i+N*j]=fabs(auxc[i]-auxp[i]);
			}
		}

		e=evec[0];
		for(i=1;i<N*(t_height);i++)
			if(evec[i]>e)
				e=evec[i];

		for(i = 0; i < N ; i++){

			border_u[i] = matc[i+N];
			border_d[i] = matc[i+N*(t_height)];
		}


		if(rank >0){
			//up ghost cells
			MPI_Irecv(ghost_u, N, MPI_DOUBLE, rank-1, 10, MPI_COMM_WORLD, &u_rec);
			MPI_Isend(border_u, N, MPI_DOUBLE, rank-1, 10, MPI_COMM_WORLD,&u_send);
		}

    	if(rank < size - 1){
    		//down ghost cells
            MPI_Irecv(ghost_d, N, MPI_DOUBLE, rank+1, 10, MPI_COMM_WORLD, &d_rec);
      		MPI_Isend(border_d, N, MPI_DOUBLE, rank+1, 10, MPI_COMM_WORLD ,&d_send);
    	}

    	//updating the ghost cells


    	if(rank == 0 && size != 1){
			MPI_Wait(&d_rec, &status);
			MPI_Wait(&d_send, &status);


			for(i = 0; i < N; i++){
				matc[i+N*(t_height+1)] = ghost_d[i];
			}
    	}
    	else if(rank == size -1 && size != 1){
			MPI_Wait( &u_rec, &status );
			MPI_Wait( &u_send, &status );


			for(i = 0; i <N; i++){
				matc[i] = ghost_u[i];
			}
    	}
    	else if(size != 1){
			MPI_Wait(&u_send, &status);
			MPI_Wait(&d_send, &status);


			MPI_Wait(&u_rec, &status);

			for(i = 1; i <N-1; i++){
				matc[i] = ghost_u[i];
			}
			MPI_Wait(&d_rec, &status);

			for(i = 1; i < N-1; i++){
				matc[i+N*(t_height+1)] = ghost_d[i];
			}
    	}
		double *temp=matc;
		matc=matp;
		matp=temp;
	it += 1;
	MPI_Barrier(MPI_COMM_WORLD); //wait for all threads to conclude before calculating the error
	MPI_Reduce(&e,&e_all,1,MPI_DOUBLE,MPI_MAX,0, MPI_COMM_WORLD);
	//fprintf(stderr, "it %d in process %d \n", it, rank);
	MPI_Bcast(&e_all,1,MPI_DOUBLE,0,MPI_COMM_WORLD); // so that all threads know where to stop	
	//fprintf(stderr, "%d broadcast \n", rank );
	//fprintf(stderr, " error %f in rank %d \n", e , rank);
	}while (e_all > emin);


	double* mat_total;
	if(rank == 0){
		mat_total = calloc(N*N,sizeof(double)); //where we'll keep the final data
	}




	

	MPI_Barrier(MPI_COMM_WORLD); //wait for all threads to conclude before gathering


	MPI_Gather(matp+N,N*(t_height),MPI_DOUBLE,mat_total+N,N*t_height,MPI_DOUBLE,0,MPI_COMM_WORLD);


	clock_gettime(CLOCK_REALTIME, &rnow);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cnow);

	cpu =(cnow.tv_nsec-cthen.tv_nsec)*1e-6+(cnow.tv_sec-cthen.tv_sec)*1e3;
	wall=(rnow.tv_nsec-rthen.tv_nsec)*1e-6+(rnow.tv_sec-rthen.tv_sec)*1e3;

	fprintf(stderr,"%.10e %.10e %d \n",cpu,wall, it);


	if(rank == 0){

		for(i=0;i<N;i++){
			mat_total[i+N*(N-1)] = (i*tmax)/((double)N-1); //the upper border is lost, so we need to reapply it
			}

		for(j=0;j<N;j++){
			for (i=0;i<N;i++){
				printf("%e ",mat_total[i+N*j]);		
			}
			printf("\n");

		}
	}


	MPI_Finalize();
}


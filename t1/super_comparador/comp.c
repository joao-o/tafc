#include <stdio.h>
#include <stdlib.h>

#define B_SZ 100000

int main(int argc, char **argv)
{
	int i,j;
	char *bnorm,*brev;
	char *nptr,*rptr;
	FILE *norm, *rev;
	int nsz=0;
	long *lines;
	double tnorm,trev;
	
	if (argc<2) {
		puts("time reversibility collision comparator\n"
				"<program> <original collision file> <reversed collision file>\n");
		exit(EXIT_FAILURE);
	}
	norm=fopen(argv[1],"r");
	rev=fopen(argv[2],"r");

	bnorm=malloc(sizeof(char)*B_SZ);
	brev=malloc(sizeof(char)*B_SZ);

	while(fgets(bnorm,B_SZ,norm)!=NULL)
		for(i=0;i<B_SZ && bnorm[i]!=0;i++)
			if(bnorm[i]=='\n')
				nsz++;

	rewind(norm);

	lines=malloc(sizeof(long)*nsz);

	for(i=0;i<nsz;i++){
		lines[i]=ftell(norm);
		do {
			fgets(bnorm,B_SZ,norm);
			for(j=0;j<B_SZ;j++)
				if(bnorm[j]=='\n') break;
		} while(j==B_SZ);
	}

	for(i=0;fgets(brev,B_SZ,rev)!=NULL||i<nsz;i++){
		fseek(norm,lines[nsz-i-1],SEEK_SET);
		fgets(bnorm,B_SZ,norm);
		printf("%d %ld ",i,strtol(brev+1,&rptr,10)-strtol(bnorm+1,&nptr,10));
		tnorm=strtod(nptr,&nptr);
		trev=strtod(rptr,&rptr);
		printf("%f %f\n",trev,tnorm+trev);
	}

}

#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.14159265358979323846264


typedef struct sheet {
/* data for a sheet */
	double x;						  /* position */
	double v;						  /* velocity */
	double eq;						  /* equilibrium position */
	int a;							  /* number of crossings (right positive, left negative) */
} sheet;

typedef struct col {
/* data for a crossing */
	double t;						  /* time */
	int a;							  /* index of left sheet */
} col;

typedef struct sysdat {
/* contains global program data */
	double wp, L;					  /* plasma frequency and length of box/ period of system */
	double t, tmax, step;		  /* time related variables */
	double vmax;					  /* maximum initial velocity when using a random distribuition */
	int nsheets;					  /* number of simulated sheets */
} sysdat;

void
die(const char *msg)
/* print msg and exit*/
{
	fputs(msg, stderr);
	exit(EXIT_FAILURE);
}

/* we use a leapfrog scheme to integrate the equations of motion because it is 
 * better at conserving the energy of the system than using the solution directly
 */
double
vel(sheet * s, sysdat * d, double step)
/* do leapfrog for velocity */
{

	return s->v * cos(d->wp*step) - d->wp * (s->x - s->eq) * sin(d->wp * step);
}

double
pos(sheet * s, sysdat * d, double step)
/* do leapfrog for position */
{
	(void)d;
	return s->x + s->v / d->wp * sin(d->wp *step) - (s->x - s->eq)*(1-cos(d->wp*step));
}

double
col_calc(sheet * a, sheet * b, sysdat * d, double step)
/* compute approximate crossing time */
{
	double num = b->x - a->x;
	double left = pos(a, d, step);
	double right = pos(b, d, step);
	return step * (num / (num + left - right));
}

double
rand_intv(double a, double b)
/* generate a random double between a and b */
{
	return a + (random() / (RAND_MAX + 1.0)) * (b - a);
}

#define B_SZ 50
void
initial_v(sheet * sheets, sysdat *d)
/* initialize velocities */
{
	int i;
	char buf[B_SZ], *endptr;
	FILE *f;
	/* takes the data from a file */
	if (d->vmax < 0) {
		if ((f = fopen("v_dist_tr", "r"))==NULL)
			die("v_dist_tr not found");
		for (i = 0; i < d->nsheets; i++) {
			fgets(buf, B_SZ, f);
			sheets[i].v = strtod(buf, &endptr);
		}
		fclose(f);

	} else {
		/* save initial velocities */
		f = fopen("v_dist", "w");
		for (i = 0; i < d->nsheets; i++) {
			sheets[i].v = rand_intv(-d->vmax, d->vmax);
			fprintf(f, "%f \n", sheets[i].v);
		}
		fclose(f);
	}

	if( d->step < 0){
		d->step=-d->step;
		for (i = 0; i < d->nsheets; i++) {
			sheets[i].v = -sheets[i].v;
		}

	}
	sheets[d->nsheets].v = sheets[0].v;

	return;
}


void
initial_x(sheet *sheets, sysdat *d)
/* initialize positions */
{
	int i;
	char buf[B_SZ], *endptr;
	FILE *f;

	/* takes the data from a file */
	if (d->vmax < 0) {
		if( (f = fopen("x_dist_tr", "r")) == NULL )
			die("x_dist_tr not found");
		for (i = 0; i < d->nsheets; i++) {
			fgets(buf, B_SZ, f);
			sheets[i].x = strtod(buf, &endptr);
			sheets[i].eq = i * d->L / (double) d->nsheets;
		}
		fclose(f);

	} else {
		for (i = 0; i < d->nsheets; i++) {
			sheets[i].x = i * d->L / (double) d->nsheets;
			sheets[i].eq = sheets[i].x;
		}
	}

	sheets[d->nsheets].x = sheets[0].x + d->L;
	sheets[d->nsheets].eq = sheets[0].eq + d->L;
	return;
}


void
init(int argc, char **argv, sysdat * dat)
/* read variables from comand line */
{
	char *endptr;

	if (argc < 6)
		die("run as:\n ./program <No of sheets> <max time> <step> <vmax> <length>\n"
			 "if vmax is negative read velocities from \"v_dist\"" 
			  "and positions from \" \" \n");

	dat->nsheets = atoi(argv[1]);
	dat->tmax = strtod(argv[2], &endptr);
	dat->step = strtod(argv[3], &endptr);
	dat->vmax = strtod(argv[4], &endptr);
	dat->L = strtod(argv[5], &endptr);

	return;
}


void
advance(sheet * sheets, sysdat * dat)
/* advance the system one step in time*/
{
	int i;
	double sv_pos;
	double tc1, tcn;
	double tmp;
	double dist;
	col c = {.a = 0,.t = 2 * dat->step };

	for (i = 0; i < dat->nsheets; i++) {
		/* compute all crossing times and find the smallest */
		tc1 = col_calc(&(sheets[i]), &(sheets[i + 1]), dat, dat->step);
		if (tc1 < c.t && tc1 > 0) {
			c.t = tc1;
			c.a = i;
		}
	}
	if (c.t < dat->step) {
		/* if the next crossing happens before the next time step handle it, 
		 * otherwise just advance the system to next time step 
		 *
		 * to compute a better crossing time the algorithm is iterated until
		 * it converges. The final distance between the 
		 * sheets is used as a stopping criterion 
		 * it is crucial that this distance is not negative 
		 * as if it is, switching the velocities of the particles to simulate a
		 * crossing will cause other crossing to happen in the next iteration
		 */
		for (dist = dat->L, i = 0; 
				fabs(dist) > (dat->L / dat->nsheets) * 1e-14 && i < 50 ; i++) {
			tcn = col_calc(&sheets[c.a], &sheets[c.a + 1], dat, c.t);
			c.t = tcn;
			dist = pos(&sheets[c.a + 1], dat, tcn) - pos(&sheets[c.a], dat, tcn);
			/*fprintf(stderr, "CI %d %d %f %.18f \n", c.a, i, dat->t + tcn, dist);*/
		}

		/* after the algorithm converges the system is advanced to the 
		 * crossing time
		 */

		for (i = 0; i < dat->nsheets + 1; i++) {
			sv_pos = pos(&(sheets[i]), dat, c.t);
			sheets[i].v = vel(&(sheets[i]), dat, c.t);
			sheets[i].x = sv_pos;
		}

		fprintf(stderr, "C %d %.18f %.18f %.18f %.18f \n",

				  c.a, dat->t+tcn, sheets[c.a + 1].x - sheets[c.a].x,
				  sheets[c.a].x, sheets[c.a + 1].x);

		/* swap the velocities */
		tmp = sheets[c.a].v;
		sheets[c.a].v = sheets[c.a + 1].v;
		sheets[c.a + 1].v = tmp;
		sheets[c.a].a++;
		sheets[c.a + 1].a--;
		dat->t = dat->t + c.t;

		/* often the algorithm above will not converge to zero distance between the 
		 * particles; if the distance is positive this is not a problem but if its 
		 * negative the above mentioned problem will ocour. To solve this, in these
		 * cases the position of the sheets is tanken as the average of their current
		 * positions
		 */
		if (dist < 0) {
			sheets[c.a].x = (sheets[c.a].x + sheets[c.a + 1].x) / 2;
			sheets[c.a + 1].x = sheets[c.a].x;
		}

		/* this sets up the mirroring between the last (virtual) sheet and the 
		 * first one, this is done to obtain periodic boundary conditions
		 */
		if (c.a == 0) {
			sheets[dat->nsheets].v = sheets[0].v;
			sheets[dat->nsheets].x = sheets[0].x + dat->L;

		} else if (c.a == dat->nsheets - 1) {
			sheets[0].v = sheets[dat->nsheets].v;
			sheets[0].x = sheets[dat->nsheets].x - dat->L;

		}

	} else {
		/* advance the system if no crossings are detected within the time step */
		for (i = 0; i < dat->nsheets + 1; i++) {
			sv_pos = pos(&(sheets[i]), dat, dat->step);
			sheets[i].v = vel(&(sheets[i]), dat, dat->step);
			sheets[i].x = sv_pos;
		}
		dat->t = dat->t + dat->step;
	}

}

int
main(int argc, char *argv[])
{
	int i, j, n;
	int sum = 0;
	double Esum = 0, E0 = 0;
	int nbins = 10;
	FILE *f,*h,*d, *o;

	sysdat dat = {.wp = 1,.t = 0.0 };

	sheet *sheets;
	unsigned int seed;

	/* this will only work on posix systems, other systems wil have to make do
	 * with 4 */
	if ((f = fopen("/dev/urandom", "r")) != NULL) {
		fread(&seed, sizeof(unsigned int), 1, f);
		fclose(f);
	} else {
		puts("could not read /dev/urandom to seed rng, seeding with 4");
		seed = 4;
	}
	srandom(seed);

	init(argc, argv, &dat);

	sheets = malloc((dat.nsheets + 1) * sizeof(sheet));

	initial_x(sheets, &dat);
	initial_v(sheets, &dat);

	/* histogram setup */

	int nper = 0;
	nper = 10 * (round(dat.tmax));
	// we take 10 samples per (1/wp)

	int flags[nper];

	for (i = 0; i < nper; i++) {
		// calculation of the various instants of time where we will sample the velocities
		flags[i] = i * dat.tmax / (nper);
	}

	int *classes = malloc(2 * nbins * sizeof(int));
	//double *velocities = malloc(nper *dat.nsheets * sizeof(double));
	double velocities[nper][dat.nsheets];


	for (i = 0; i < nbins * 2; i++)	// initialization of the bins for the histogram
		classes[i] = 0;
	for (i = 0; i < dat.nsheets; i++){
		E0 += sheets[i].v * sheets[i].v +
			 dat.wp * dat.wp * (sheets[i].x - sheets[i].eq) * (sheets[i].x -
																				sheets[i].eq);
	}
	n = 0;
	while (dat.t < dat.tmax) {		//MAIN LOOP

		advance(sheets, &dat);
		printf("%g ", dat.t);
		if (dat.t > flags[n] && n < nper) {
			for (j = 0; j < dat.nsheets; j++){
				//velocities[n*nper + j] = sheets[j].v;
				velocities[n][j] = sheets[j].v;
			}
			n++;
		}

		for (i = 0; i < dat.nsheets; i++)
			printf("%f ", sheets[i].x);
		Esum = 0;
		for (i = 0; i < dat.nsheets; i++)
			Esum += (sheets[i].v * sheets[i].v +
						dat.wp * (sheets[i].x - sheets[i].eq) *
						dat.wp * (sheets[i].x - sheets[i].eq)) / E0;
		printf("%.18f ",Esum - 1.0);

		printf("\n");
	}
	for (i = 0; i < dat.nsheets; i++) {
		sum = sum + sheets[i].a;
	}
	//fprintf(stderr, " hahahah %d \n", sum);
	double vmax = 0;
	double Ec = 0;

	for (n = 0; n < nper; n++) {
		for (j = 0; j < dat.nsheets; j++) {
			//Ec += velocities[n*nper + j]*velocities[n*nper + j];
			Ec += velocities[n][j]*velocities[n][j];
			if (fabs(velocities[n][j]) > vmax){
				//vmax = fabs(velocities[n*nper + j]);
				vmax = fabs(velocities[n][j]);
			}
		}
	}

	double classL = vmax / nbins;	//creation of the histogram : 

	//calculation of the variation of the second momemtum
	int norm = 0;
	int k = 0;
	double p = 0;
	double norm1 = 0;
	o = fopen("pvar.dat", "w");

	for(k = 0; k < nper/10 ;k++){

		for (n = k; n < 10 + k*10; n++) {
			for (j = 0; j < dat.nsheets; j++) {
				//classes[(int)floor(velocities[n*nper + j] / classL) + nbins]++;
				classes[(int)floor(velocities[n][j] / classL) + nbins]++;
				//fprintf(stderr, "%d %d \n", n, j );
			}
		}
		norm1 = 0; 
		for(i = 0; i < 2*nbins; i++){
				norm1 += classes[i];
		}

		
		p = 0;
		for (i = 0; i < 2 * nbins; i++) {
			p += ((i * classL) - vmax) * 
				  ((i * classL) - vmax) *classes[i]/ (norm1);	
					  //second momenta calculation

		}
		fprintf(o, "%f\n" , p);

		


		for (n = 0; n < nper; n++) {			
			for (j = 0; j < dat.nsheets; j++) {
				//classes[(int)floor(velocities[n*nper + j] / classL) + nbins]++;
				classes[(int)floor(velocities[n][j] / classL) + nbins] = 0;
			}
		}


	}

	//end of the calculation



	f = fopen("hist.dat", "w");
	h = fopen("v_dist_tr", "w");
	d = fopen("x_dist_tr", "w");

	for (n = 0; n < nper; n++) {
		for (j = 0; j < dat.nsheets; j++) {
			classes[(int)floor(velocities[n][j] / classL) + nbins]++;
		}
	}

	for(i = 0; i < 2*nbins; i++){
		norm += classes[i];
	}
	fprintf(stderr, "%d %d \n", norm , (dat.nsheets * nper) );


	p = 0;
	for (i = 0; i < 2 * nbins; i++) {
		fprintf(f, "%f %d \n" , (i * classL) - vmax, classes[i]);  //data to fit later
		p += ((i * classL) - vmax) * 
			  ((i * classL) - vmax) *classes[i]/ (norm);	
				  //second momenta calculation
	}
	

	fprintf(stderr, "p = %f \n", p);	
	fprintf(stderr, " vmax %f \n", vmax);

	fprintf(stderr, "FIM\n");	
	

	for(i=0; i< dat.nsheets; i++){
	fprintf(h, "%f\n", sheets[i].v);

	fprintf(d, "%f\n", sheets[i].x);	
	}
}

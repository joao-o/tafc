set terminal png size 800,600
set output "out.png"
set grid
set xlabel "t [wp^-1]"
set ylabel "Fração de energia perdida"
plot for [i=start:end] filename u 1:i with lines notitle

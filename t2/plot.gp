set terminal png size 800,600
out=sprintf("%s.png",filename)
set output out

set grid
set xlabel "x"
set ylabel "psi"

plot filename u 1:2 with lines notitle, filename u 1:3 w lines notitle

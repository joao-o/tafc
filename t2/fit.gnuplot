set terminal png size 800,600
data="res-1.00000e-01"
out="res-1.00000e-01.png"

set output out
set samples 1000

set grid

f(x)=A*cos(sqrt(2*E)*(x-L))
A=2.02611;
E=0.1
L=7;

set mxtics 5
set xrange [100:200]

fit [160:500][] f(x) data u 1:2 via L,A
plot data u 1:2 notitle ,f(x) notitle lw 3 

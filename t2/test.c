#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include <math.h>

int
func(double t, const double y[],double f[], void *params)
{
	double E = *(double*)params;
	f[0] = y[1];
	f[1] = -10.0*cos(t)*cos(t)*y[0]-2*E*y[0];
	return GSL_SUCCESS;
}

int
jac(double t, const double y[],double *dfdy, double dfdt[],void *params)
{
	double E = *(double*)params;
	gsl_matrix_view dfdy_mat=gsl_matrix_view_array (dfdy, 2, 2);
	gsl_matrix * m = &dfdy_mat.matrix;
	gsl_matrix_set (m, 0, 0, 0.0);
	gsl_matrix_set (m, 0, 1, 1.0);
	gsl_matrix_set (m, 1, 0, -10.0*cos(t)*cos(t) -2.0*E);
	gsl_matrix_set (m, 1, 1, 0);
	dfdt[0] = 0.0;
	dfdt[1] = 20.0*cos(t)*sin(t)*y[0];
	return GSL_SUCCESS;
}

int
func2(double t, const double y[],double f[], void *params)
{
	double E = *(double*)params;
	f[0] = y[1];
	f[1] = -2.0*exp(-t*t/8.0)*y[0]-2.0*E*y[0];
	return GSL_SUCCESS;
}

int
jac2(double t, const double y[],double *dfdy, double dfdt[],void *params)
{
	double E = *(double*)params;
	gsl_matrix_view dfdy_mat=gsl_matrix_view_array (dfdy, 2, 2);
	gsl_matrix * m = &dfdy_mat.matrix;
	gsl_matrix_set (m, 0, 0, 0.0);
	gsl_matrix_set (m, 0, 1, 1.0);
	gsl_matrix_set (m, 1, 0, -2.0*exp(-t*t/8) -2.0*E);
	gsl_matrix_set (m, 1, 1, 0);
	dfdt[0] = 0.0;
	dfdt[1] = t*exp(-t*t/8.0)/2.0*y[0];
	return GSL_SUCCESS;
}

#define B_SZ 100

int 
main(int argc,char **argv)
{
	char buf[B_SZ];
	double e0,e1,E;
	double t=0.0, t1;
	double y[2] = { 1.0, 0.0 };
	int i,j,tsts,ests;
	FILE *f;

	if(argc<6){
		puts("not enough args\n <maxt> <tsteps> <estart> <estop> <esteps>");
		exit(EXIT_FAILURE);
	}

	t1=strtod(argv[1],NULL);
	tsts=strtod(argv[2],NULL);
	e0=strtod(argv[3],NULL);
	e1=strtod(argv[4],NULL);
	ests=strtod(argv[5],NULL);

	gsl_odeiv2_system sys = {func2,jac2,2,&E};
	gsl_odeiv2_driver * d = gsl_odeiv2_driver_alloc_y_new (
     &sys, gsl_odeiv2_step_rk8pd, 1e-6, 1e-6, 0);

	for (j = 0; j<=ests; j++ ){
		E = e0 + j * (e1-e0) / (double) ests;
		y[0] = 1.5; 
		y[1] = 0.0;
		t=0;
		snprintf(buf,B_SZ,"/dev/shm/res-%.5e",E);
		f=fopen(buf,"w");
		for (i = 0; i <tsts ; i++) {
			double ti = 0.0 + i * t1 / (double) tsts;
      	int status = gsl_odeiv2_driver_apply (d, &t, ti, y);

	      if (status != GSL_SUCCESS) {
   	       fprintf (stderr,"error, return value=%d\n", status);
      	    break;
			}

			fprintf (f,"%.5e %.5e %.5e\n", t, y[0], -exp(-ti*ti/8));
		}
		gsl_odeiv2_driver_reset(d);
		fclose(f);
	}

  gsl_odeiv2_driver_free (d);
  return 0;
}

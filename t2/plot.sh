#!/bin/sh

wd=$(pwd)
cd /dev/shm
for i in res*[^g]
do	
	echo $i
	gnuplot -e "filename=\"$i\"" $wd/plot.gp
done
sxiv *png
cd $wd
